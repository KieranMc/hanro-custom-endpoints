<?php
/*
Plugin Name: Hanro Custom Endpoints
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Basic WordPress Plugin Header Comment
Version:     1.0.2
Author:      WordPress.org
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/
/*
 * Add custom endpoint that appears in My Account Page - WooCommerce 2.6
 * New URL below as Claudio changed his github username
 * Ref - https://gist.github.com/claudiosanches/a79f4e3992ae96cb821d3b357834a005#file-custom-my-account-endpoint-php
 */
class My_Custom_My_Account_Endpoint {
 /**
 * Custom endpoint name.
 *
 * @var string
 */
 public static $endpoint = 'my-wishlist';
 /**
 * Plugin actions.
 */
 public function __construct() {
 // Actions used to insert a new endpoint in the WordPress.
 add_action( 'init', array( $this, 'add_endpoints' ) );
 add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
 // Change the My Accout page title.
 add_filter( 'the_title', array( $this, 'endpoint_title' ) );
 // Insering your new tab/page into the My Account page.
 add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
 add_action( 'woocommerce_account_' . self::$endpoint . '_endpoint', array( $this, 'endpoint_content' ) );
 }
 /**
 * Register new endpoint to use inside My Account page.
 *
 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
 */
 public function add_endpoints() {
 add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
 }
 /**
 * Add new query var.
 *
 * @param array $vars
 * @return array
 */
 public function add_query_vars( $vars ) {
 $vars[] = self::$endpoint;
 return $vars;
 }
 /**
 * Set endpoint title.
 *
 * @param string $title
 * @return string
 */
 public function endpoint_title( $title ) {
 global $wp_query;
 $is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );
 if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
 // New page title.
 $title = __( 'My Wishlist', 'woocommerce' );
 // I had to remove below title as it was removing the endpoint filter twice and obstructing woocommerce.php code
 // remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
 }
 return $title;
 }
 /**
 * Insert the new endpoint into the My Account menu.
 *
 * @param array $items
 * @return array
 */
 public function new_menu_items( $items ) {
 // Remove the logout menu item.
 $logout = $items['customer-logout'];
 unset( $items['customer-logout'] );
 // Insert your custom endpoint.
 $items[ self::$endpoint ] = __( 'My Wishlist', 'woocommerce' );
 // Insert back the logout item.
 $items['customer-logout'] = $logout;
 return $items;
 }
 /**
 * Endpoint HTML content.
 */
 public function endpoint_content() {
 // wc_get_template( 'myaccount/navigation.php' );
 ?>
 <div class="">
 	<?php echo do_shortcode('[yith_wcwl_wishlist]'); ?>
 </div>
 <?php
 }
 /**
 * Plugin install action.
 * Flush rewrite rules to make our custom endpoint available.
 */
 public static function install() {
 flush_rewrite_rules();
 }
}
new My_Custom_My_Account_Endpoint();
// Flush rewrite rules on plugin activation.
register_activation_hook( __FILE__, array( 'My_Custom_My_Account_Endpoint', 'install' ) );








class My_Delivery_Custom_Endpoint {
 /**
 * Custom endpoint name.
 *
 * @var string
 */
 public static $endpoint = 'delivery-and-returns';
 /**
 * Plugin actions.
 */
 public function __construct() {
 // Actions used to insert a new endpoint in the WordPress.
 add_action( 'init', array( $this, 'add_endpoints' ) );
 add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
 // Change the My Accout page title.
 add_filter( 'the_title', array( $this, 'endpoint_title' ) );
 // Insering your new tab/page into the My Account page.
 add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
 add_action( 'woocommerce_account_' . self::$endpoint . '_endpoint', array( $this, 'endpoint_content' ) );
 }
 /**
 * Register new endpoint to use inside My Account page.
 *
 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
 */
 public function add_endpoints() {
 add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
 }
 /**
 * Add new query var.
 *
 * @param array $vars
 * @return array
 */
 public function add_query_vars( $vars ) {
 $vars[] = self::$endpoint;
 return $vars;
 }
 /**
 * Set endpoint title.
 *
 * @param string $title
 * @return string
 */
 public function endpoint_title( $title ) {
 global $wp_query;
 $is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );
 if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
 // New page title.
 $title = __( 'Delivery & Returns', 'woocommerce' );
 // I had to remove below title as it was removing the endpoint filter twice and obstructing woocommerce.php code
 // remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
 }
 return $title;
 }
 /**
 * Insert the new endpoint into the My Account menu.
 *
 * @param array $items
 * @return array
 */
 public function new_menu_items( $items ) {
 // Remove the logout menu item.
$wishlist = $items['my-wishlist'];
 unset( $items['my-wishlist'] );
 // Insert your custom endpoint.
 $items[ self::$endpoint ] = __( 'Delivery & Returns', 'woocommerce' );
 // Insert back the logout item.
 $items['my-wishlist'] = $wishlist;
 return $items;
 }
 /**
 * Endpoint HTML content.
 */
 public function endpoint_content() {
	global $post;
	$post = get_post( 8476 );
	setup_postdata( $post );
	get_template_part( 'templates/content-single-article' );
	wp_reset_postdata();
 ?>

 <?php
 }
 /**
 * Plugin install action.
 * Flush rewrite rules to make our custom endpoint available.
 */
 public static function install() {
 flush_rewrite_rules();
 }
}
new My_Delivery_Custom_Endpoint();
// Flush rewrite rules on plugin activation.
register_activation_hook( __FILE__, array( 'My_Delivery_Custom_Endpoint', 'install' ) );